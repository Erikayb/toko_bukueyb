import 'dart:convert';


List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({
    this.Nama_Barang,
    this.Deskripsi,
    this.Harga,
    this.url_gambar,
   
  });

  String Nama_Barang;
  String Deskripsi;
  String Harga;
  String url_gambar;
  

  Map toJson() => {
        "Nama_Barang": Nama_Barang,
        "Deskripsi": Deskripsi,
        "Harga": Harga,
        "url_gambar": url_gambar,
        
      };
}
