import 'package:flutter/material.dart';
import 'package:toko_buku/model/jsonmodel.dart';
import 'package:toko_buku/model/viewmodel.dart';
import 'package:toko_buku/screen/home_page.dart';
class Entryform extends StatefulWidget {
  Entryform({Key key}) : super(key: key);

  @override
  _EntryformState createState() => _EntryformState();
}

class _EntryformState extends State<Entryform> {

  final TextEditingController _namabarang = new TextEditingController();
  final TextEditingController _harga = new TextEditingController();
  final TextEditingController _deskripsi = new TextEditingController();
  final TextEditingController _urlgambar = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: Container(
        margin: EdgeInsets.all(20),
        child: Column(children: [
         Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: "Nama Barang",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => !input.contains("@")
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: "Harga Barang",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => !input.contains("@")
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: "URL Gambar",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => input.contains("@")
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: "Deskripsi",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => input.contains("@")
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      
                      Container(
                        child: InkWell(
                          onTap: (){
                             UserpostModel commRequest = UserpostModel();

    commRequest.Nama_Barang= _namabarang.text;
    commRequest.Harga= _harga.text;
    commRequest.Deskripsi= _deskripsi.text;
    commRequest.url_gambar= _urlgambar.text;
    

    UserViewModel()
        .postUser(userpostModelToJson(commRequest))
        .then((value) => print('success'));
    
              showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('TERIMAKASIH'),
                                  content: Container(
                                    child: Text('Data barang sudah diinput.: ' +
                                        _namabarang.text +
                                        " " +
                                        _harga.text +
                                        " " +
                                        _deskripsi.text +
                                        " " +
                                        _urlgambar.text
                                        ),
                                  ),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyHomePage()));
                                        },
                                        child: Text('close'))
                                  ],
                                );
                              });
  
                          },
                          child: Container(
                            margin: EdgeInsets.all(0),
                            width: double.infinity,
                            height: 70,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.lightBlue),
                            child: Center(child: Text("Add", style: TextStyle(color: Colors.white),)),
                          ),
                        ),
                      ),
      ],),)),
    );
  }
}